module gitlab.com/random-text/api

go 1.16

require (
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.6.1
	github.com/graphql-go/graphql v0.7.9
	github.com/graphql-go/handler v0.2.3
	github.com/streadway/amqp v1.0.0
	go.mongodb.org/mongo-driver v1.5.3
)
