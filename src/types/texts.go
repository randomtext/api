package types

import "github.com/graphql-go/graphql"

var Text = graphql.NewObject(graphql.ObjectConfig{
	Name: "Text",
	Fields: graphql.Fields{
		"id":              &graphql.Field{Type: ObjectID},
		"message":         &graphql.Field{Type: graphql.String},
		"author_id":       &graphql.Field{Type: graphql.String},
		"graphql_status":  &graphql.Field{Type: graphql.String},
		"rabbitmq_status": &graphql.Field{Type: graphql.String},
		"grpc_status":     &graphql.Field{Type: graphql.String},
	},
})
