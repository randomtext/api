package types

import (
	"github.com/graphql-go/graphql"
)

var Success = graphql.NewObject(graphql.ObjectConfig{
	Name: "Success",
	Fields: graphql.Fields{
		"success": &graphql.Field{Type: graphql.String},
	},
})
