package utils

import (
	"log"
	"os"
	"runtime"
	"strconv"
	"time"
)

func Log(isError bool, message string) {
	// ISO_DATE [SEVERITY] [SERVER_ID] [SERVICE] [FILE] [LINE] MESSAGE
	reset := "\033[0m"
	red := "\033[31m"
	green := "\033[32m"
	severity := "INFO"
	color := green
	if runtime.GOOS == "windows" {
		red = ""
		green = ""
	}
	if isError {
		severity = "ERROR"
		color = red
	}
	_, file, line, _ := runtime.Caller(1)
	log.Println(color + time.Now().UTC().Format(time.RFC3339) +
		" " +
		"[" + severity + "]" +
		" " +
		"[" + os.Getenv("SERVER_ID") + "]" +
		" " +
		"[" + "API" + "]" +
		" " +
		"[" + file + "]" +
		" " +
		"[" + strconv.Itoa(line) + "]" +
		" " +
		message + reset)
}
