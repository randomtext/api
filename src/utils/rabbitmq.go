package utils

import (
	"bytes"
	"encoding/json"
	"gitlab.com/random-text/api/src/payloads"
)

func Serialize(p *payloads.Text) ([]byte, error) {
	var b bytes.Buffer
	encoder := json.NewEncoder(&b)
	err := encoder.Encode(p)
	return b.Bytes(), err
}
