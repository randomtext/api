package utils

import "go.mongodb.org/mongo-driver/bson"

func WhiteListForWatch(fields []string) []bson.M {
	var pipeline []bson.M
	pipeline = append(pipeline, bson.M{"$match": bson.M{"operationType": bson.M{"$in": []interface{}{"insert", "replace", "update", "delete"}}}})
	fieldsWhitelisted := make(map[string]interface{})
	fieldsWhitelisted["operationType"] = 1
	fieldsWhitelisted["documentKey"] = 1
	fieldsWhitelisted["fullDocument"] = 1
	for _, v := range fields {
		fieldsWhitelisted[v] = 1
	}
	return append(pipeline, bson.M{"$project": fieldsWhitelisted})
}
