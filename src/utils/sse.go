package utils

import (
	"encoding/json"
	"gitlab.com/random-text/api/src/responses"
	"net/http"
)

const (
	textsColl = "texts"
)

func InitSubData(r *http.Request) *responses.SubscriberData {
	subData := &responses.SubscriberData{}
	collNames, ok := r.URL.Query()["collName"]
	if !ok {
		return nil
	}
	collName := collNames[0]
	if collName != textsColl {
		return nil
	}
	subData.CollName = collName
	authorIDs, ok := r.URL.Query()["authorID"]
	if ok {
		subData.AuthorID = authorIDs[0]
	}
	return subData
}

func sendData(doc *responses.WatchData, c chan []byte) error {
	r, err := json.Marshal(doc)
	if err != nil {
		return err
	}
	select {
	case c <- r:
	default:
	}
	return nil
}

func RoutingData(doc *responses.WatchData, c chan []byte, subData *responses.SubscriberData) error {
	if doc.CollName == textsColl && subData.CollName == textsColl {
		if subData.AuthorID == doc.Text.AuthorID {
			if err := sendData(doc, c); err != nil {
				return err
			}
		}
	}
	return nil
}
