package mutations

import "gitlab.com/random-text/api/src/payloads"

type UseCase interface {
	NewText(p *payloads.Text) (bool, error)
}
