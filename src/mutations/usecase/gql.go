package usecase

import (
	"github.com/streadway/amqp"
	"gitlab.com/random-text/api/src/mutations"
	"gitlab.com/random-text/api/src/payloads"
	"gitlab.com/random-text/api/src/utils"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
	"time"
)

type useCase struct {
	R               mutations.Repository
	RabbitMQChannel *amqp.Channel
}

func New(repo mutations.Repository, rabbitmqCh *amqp.Channel) mutations.UseCase {
	return &useCase{R: repo, RabbitMQChannel: rabbitmqCh}
}

func (uc *useCase) NewText(p *payloads.Text) (bool, error) {
	id, err := uc.R.InsertText(p)
	if err != nil {
		utils.Log(true, err.Error())
		return false, utils.Error(http.StatusInternalServerError, "something went wrong", map[string]string{})
	}
	time.Sleep(time.Second)
	p.ID = id
	err = uc.publishRabbitMQMessage(p)
	if err != nil {
		utils.Log(true, err.Error())
		filter := bson.M{"_id": p.ID}
		update := bson.M{"rabbitmq_status": "RabbitMQ send fail"}
		err = uc.R.UpdateTextStatus(filter, update)
		if err != nil {
			utils.Log(true, err.Error())
		}
		return false, utils.Error(http.StatusInternalServerError, "something went wrong", map[string]string{})
	}

	utils.Log(false, "newText success")
	return true, nil
}

func (uc *useCase) publishRabbitMQMessage(p *payloads.Text) error {
	rabbitMQPayload, err := utils.Serialize(p)
	if err != nil {
		return err
	}
	err = uc.RabbitMQChannel.Publish(
		"logs_direct",  // exchange
		"random_texts", // routing key
		false,          // mandatory
		false,          // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        rabbitMQPayload,
		})
	if err != nil {
		return err
	}
	return nil
}
