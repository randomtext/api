package repository

import (
	"context"
	"gitlab.com/random-text/api/src/models"
	"gitlab.com/random-text/api/src/mutations"
	"gitlab.com/random-text/api/src/payloads"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"os"
)

type repository struct {
	C *mongo.Client
}

func New(c *mongo.Client) mutations.Repository {
	return &repository{C: c}
}

func (r *repository) InsertText(p *payloads.Text) (primitive.ObjectID, error) {
	coll := r.C.Database(os.Getenv("DATABASE")).Collection("texts")
	id, err := coll.InsertOne(context.TODO(), &models.Text{
		Message:        p.Message,
		AuthorID:       p.AuthorID,
		GraphqlStatus:  "GraphQL ok",
		RabbitMQStatus: "",
		RedisStatus:    "",
		GrpcStatus:     "",
	})
	if err != nil {
		return primitive.ObjectID{}, err
	}
	return id.InsertedID.(primitive.ObjectID), nil
}

func (r *repository) UpdateTextStatus(filter, update bson.M) error {
	coll := r.C.Database(os.Getenv("DATABASE")).Collection("texts")
	_, err := coll.UpdateOne(context.TODO(), filter, bson.M{"$set": update})
	if err != nil {
		return err
	}
	return nil
}
