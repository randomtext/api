package gql

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/random-text/api/src/mutations"
	"gitlab.com/random-text/api/src/payloads"
	"gitlab.com/random-text/api/src/responses"
	"gitlab.com/random-text/api/src/types"
	"gitlab.com/random-text/api/src/utils"
)

type handler struct {
	UC mutations.UseCase
}

func New(uc mutations.UseCase) graphql.Fields {
	h := &handler{UC: uc}
	return graphql.Fields{
		"addText": h.newText(),
	}
}

func (h *handler) newText() *graphql.Field {
	return &graphql.Field{
		Type: types.Success,
		Args: graphql.FieldConfigArgument{
			"message": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"author_id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			text := &payloads.Text{
				Message:  p.Args["message"].(string),
				AuthorID: p.Args["author_id"].(string),
			}
			if validation := utils.Validate(text, nil); validation != nil {
				utils.Log(true, validation.Error())
				return &responses.Success{Success: false}, validation
			}
			success, err := h.UC.NewText(text)
			utils.Log(false, "NewText")
			return &responses.Success{Success: success}, err
		},
	}
}
