package mutations

import (
	"gitlab.com/random-text/api/src/payloads"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Repository interface {
	InsertText(p *payloads.Text) (primitive.ObjectID, error)
	UpdateTextStatus(filter, update bson.M) error
}
