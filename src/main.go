package main

import (
	"context"
	"fmt"
	"github.com/graphql-go/graphql"
	"github.com/streadway/amqp"
	"gitlab.com/random-text/api/src/connections"
	"gitlab.com/random-text/api/src/connections/sse"
	"gitlab.com/random-text/api/src/connections/sse/notificationCenter"
	"gitlab.com/random-text/api/src/mutations/delivery/gql"
	"gitlab.com/random-text/api/src/mutations/repository"
	"gitlab.com/random-text/api/src/mutations/usecase"
	gql2 "gitlab.com/random-text/api/src/queries/delivery/gql"
	repository2 "gitlab.com/random-text/api/src/queries/repository"
	usecase2 "gitlab.com/random-text/api/src/queries/usecase"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"net/http"
	"os"
)

func initMutations(c *mongo.Client, rabbitmqCh *amqp.Channel) graphql.Fields {
	repo := repository.New(c)
	useCase := usecase.New(repo, rabbitmqCh)
	return gql.New(useCase)
}

func initQueries(c *mongo.Client) graphql.Fields {
	repo := repository2.New(c)
	useCase := usecase2.New(repo)
	return gql2.New(useCase)
}

func initSchemaGql(c *mongo.Client, rabbitmqCh *amqp.Channel) graphql.Schema {
	rootMutation := graphql.ObjectConfig{Name: "Mutation", Fields: initMutations(c, rabbitmqCh)}
	rootQuery := graphql.ObjectConfig{Name: "Query", Fields: initQueries(c)}
	schemaConfig := graphql.SchemaConfig{Query: graphql.NewObject(rootQuery), Mutation: graphql.NewObject(rootMutation)}
	schema, err := graphql.NewSchema(schemaConfig)
	if err != nil {
		log.Fatalf("failed to create new schema, error: %v", err)
	}
	return schema
}

func sseInit(c *mongo.Client, n sse.Notifier) {
	repo := repository2.New(c)
	useCase := usecase2.New(repo)
	gql2.NewSSE(useCase, n)
}

func main() {
	c, err := connections.MongoInit()
	if err != nil {
		panic(err)
	}
	rabbitmqC, rabbitmqCh, err := connections.RabbitMQConnect()
	if err != nil {
		panic(err)
	}
	err = connections.DeclareExchange(rabbitmqCh)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := c.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
		if err := rabbitmqC.Close(); err != nil {
			panic(err)
		}
		if err := rabbitmqCh.Close(); err != nil {
			panic(err)
		}
	}()
	schema := initSchemaGql(c, rabbitmqCh)
	connections.InjectGraphqlHandler(schema)

	nc := notificationCenter.New()
	sseInit(c, nc)
	http.Handle("/subscriptions", sse.HandleSSE(nc))

	if os.Getenv("ENVIRONMENT") == "production" {
		fmt.Println("server is running in production mode on port " + os.Getenv("PORT"))
		fmt.Println("rabbitmq is running in production mode on port " + os.Getenv("RABBITMQ_PORT"))
		if err := http.ListenAndServeTLS(":"+os.Getenv("PORT"), os.Getenv("FILE_PATH"), os.Getenv("KEY_PATH"), nil); err != nil {
			panic(err)
		}
	}

	fmt.Println("server is running in development mode on port " + os.Getenv("PORT"))
	fmt.Println("rabbitmq is running in development mode on port " + os.Getenv("RABBITMQ_PORT"))
	if err := http.ListenAndServe(":"+os.Getenv("PORT"), nil); err != nil {
		panic(err)
	}
}
