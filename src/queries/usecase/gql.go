package usecase

import (
	"context"
	"fmt"
	"gitlab.com/random-text/api/src/queries"
	"gitlab.com/random-text/api/src/responses"
	"gitlab.com/random-text/api/src/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

const (
	textsColl = "texts"
)

type usecase struct {
	R queries.Repository
}

func New(r queries.Repository) queries.UseCase {
	return &usecase{R: r}
}

func (uc *usecase) GetRandom() (*responses.Text, error) {
	text, err := uc.R.GetText()
	if err != nil {
		utils.Log(true, err.Error())
		return nil, utils.Error(http.StatusInternalServerError, "something went wrong", map[string]string{})
	}
	utils.Log(false, "getRandomText success")
	return text, nil
}

func (uc *usecase) ObserveTexts() (<-chan *responses.WatchData, error) {
	w, err := uc.R.ObserveTexts()
	if err != nil {
		return nil, err
	}
	var doc bson.M
	result := &responses.WatchData{}
	out := make(chan *responses.WatchData)
	go func() {
		for w.Next(context.TODO()) {
			if e := w.Decode(&doc); e != nil {
				panic(e)
			}
			id := doc["documentKey"].(primitive.M)["_id"].(primitive.ObjectID)
			operationType := fmt.Sprintf("%v", doc["operationType"])
			text := &responses.Text{}
			if operationType == "delete" {
				text.ID = id
			} else {
				text, err = uc.R.GetTextByID(id)
				if err != nil {
					panic(err)
				}
			}
			result.Text = text
			result.OperationType = operationType
			result.CollName = textsColl
			out <- result
			doc = nil
		}
		close(out)
	}()
	return out, nil
}
