package repository

import (
	"context"
	"errors"
	"gitlab.com/random-text/api/src/queries"
	"gitlab.com/random-text/api/src/responses"
	"gitlab.com/random-text/api/src/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"math/rand"
	"os"
)

type repository struct {
	C *mongo.Client
}

func New(c *mongo.Client) queries.Repository {
	return &repository{C: c}
}

func (r *repository) GetText() (*responses.Text, error) {
	coll := r.C.Database(os.Getenv("DATABASE")).Collection("texts")
	itemCount, err := coll.CountDocuments(context.TODO(), bson.M{})
	if err != nil {
		return nil, err
	}
	if itemCount == 0 {
		return &responses.Text{Message: ""}, nil
	}
	count := int(itemCount)
	randItem := rand.Intn(count)
	if randItem == count {
		randItem = 0
	}
	pipeline := []bson.M{
		{"$skip": randItem},
		{"$limit": 1},
	}
	cursor, err := coll.Aggregate(context.TODO(), pipeline)
	if err != nil {
		return nil, err
	}
	var results []*responses.Text
	if err := cursor.All(context.TODO(), &results); err != nil {
		return nil, err
	}
	if len(results) == 0 {
		return nil, errors.New("text not found")
	}
	return results[0], nil
}

func (r *repository) GetTextByID(id primitive.ObjectID) (*responses.Text, error) {
	coll := r.C.Database(os.Getenv("DATABASE")).Collection("texts")
	var result *responses.Text
	err := coll.FindOne(context.TODO(), bson.M{"_id": id}).Decode(&result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (r *repository) ObserveTexts() (*mongo.ChangeStream, error) {
	coll := r.C.Database(os.Getenv("DATABASE")).Collection("texts")
	o := options.ChangeStream().SetFullDocument(options.UpdateLookup)
	pipeline := utils.WhiteListForWatch([]string{"_id, author_id, message"})
	w, err := coll.Watch(context.TODO(), pipeline, o)
	if err != nil {
		return nil, err
	}
	return w, nil
}
