package queries

import (
	"gitlab.com/random-text/api/src/responses"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Repository interface {
	GetText() (*responses.Text, error)
	GetTextByID(id primitive.ObjectID) (*responses.Text, error)
	ObserveTexts() (*mongo.ChangeStream, error)
}
