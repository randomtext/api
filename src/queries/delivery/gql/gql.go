package gql

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/random-text/api/src/queries"
	"gitlab.com/random-text/api/src/types"
)

type Handler struct {
	UC queries.UseCase
}

func New(uc queries.UseCase) graphql.Fields {
	h := &Handler{UC: uc}
	return graphql.Fields{
		"getRandomText": h.getRandomText(),
	}
}

func (h *Handler) getRandomText() *graphql.Field {
	return &graphql.Field{
		Type: types.Text,
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			text, err := h.UC.GetRandom()
			if err != nil {
				return nil, err
			}
			return text, nil
		},
	}
}
