package gql

import (
	"fmt"
	"gitlab.com/random-text/api/src/connections/sse"
	"gitlab.com/random-text/api/src/queries"
	"runtime"
)

func NewSSE(uc queries.UseCase, n sse.Notifier) {
	h := &Handler{UC: uc}
	h.Observe(n)
}

func (h *Handler) Observe(n sse.Notifier) {
	go func() {
		c, err := h.UC.ObserveTexts()
		if err != nil {

		}
		for v := range c {
			fmt.Println("Routines number:", runtime.NumGoroutine())
			err := n.Notify(v)
			if err != nil {
				panic(err)
			}
		}
	}()
}
