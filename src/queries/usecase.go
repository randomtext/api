package queries

import "gitlab.com/random-text/api/src/responses"

type UseCase interface {
	GetRandom() (*responses.Text, error)
	ObserveTexts() (<-chan *responses.WatchData, error)
}
