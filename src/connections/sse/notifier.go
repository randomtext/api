package sse

import "gitlab.com/random-text/api/src/responses"

type Notifier interface {
	Notify(doc *responses.WatchData) error
}
