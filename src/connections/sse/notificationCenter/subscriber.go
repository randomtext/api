package notificationCenter

import (
	"gitlab.com/random-text/api/src/connections/sse"
	"gitlab.com/random-text/api/src/responses"
	"gitlab.com/random-text/api/src/utils"
	"sync"
)

type Subscriber struct {
	subscribers   map[chan []byte]*responses.SubscriberData
	subscribersMu *sync.Mutex
}

func New() *Subscriber {
	return &Subscriber{
		subscribers:   map[chan []byte]*responses.SubscriberData{},
		subscribersMu: &sync.Mutex{},
	}
}

func (nc *Subscriber) Subscribe(c chan []byte, subData *responses.SubscriberData) (sse.UnsubscribeFunc, error) {
	nc.subscribersMu.Lock()
	nc.subscribers[c] = subData
	nc.subscribersMu.Unlock()
	unsubscribeFn := func() error {
		nc.subscribersMu.Lock()
		delete(nc.subscribers, c)
		nc.subscribersMu.Unlock()
		return nil
	}
	return unsubscribeFn, nil
}
func (nc *Subscriber) Notify(doc *responses.WatchData) error {
	nc.subscribersMu.Lock()
	defer nc.subscribersMu.Unlock()
	for c, subData := range nc.subscribers {
		if err := utils.RoutingData(doc, c, subData); err != nil {
			return err
		}
	}
	return nil
}
