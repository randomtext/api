package sse

import "gitlab.com/random-text/api/src/responses"

type UnsubscribeFunc func() error

type Subscriber interface {
	Subscribe(c chan []byte, data *responses.SubscriberData) (UnsubscribeFunc, error)
}
