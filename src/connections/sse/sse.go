package sse

import (
	"context"
	"fmt"
	"gitlab.com/random-text/api/src/utils"
	"net/http"
)

func HandleSSE(s Subscriber) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		subData := utils.InitSubData(r)
		if subData == nil {
			return
		}
		// Subscribe
		c := make(chan []byte)
		unsubscribeFn, err := s.Subscribe(c, subData)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		// Signal SSE Support
		w.Header().Set("Content-Type", "text/event-stream")
		w.Header().Set("Cache-Control", "no-cache")
		w.Header().Set("Connection", "keep-alive")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		ctx := context.TODO()
		r = r.WithContext(ctx)

	Looping:
		for {
			select {
			case <-ctx.Done():
				if err := unsubscribeFn(); err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				break Looping
			default:
				_, _ = fmt.Fprintf(w, "data: %s\n\n", <-c)
				w.(http.Flusher).Flush()
			}
		}
	}
}
