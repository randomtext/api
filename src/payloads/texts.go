package payloads

import "go.mongodb.org/mongo-driver/bson/primitive"

type Text struct {
	ID       primitive.ObjectID `json:"id" bson:"_id"`
	Message  string             `json:"message" bson:"message" validate:"required"`
	AuthorID string             `json:"author_id" bson:"author_id" validate:"required"`
}
