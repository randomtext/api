package responses

import "go.mongodb.org/mongo-driver/bson/primitive"

type Text struct {
	ID             primitive.ObjectID `json:"id" bson:"_id"`
	Message        string             `json:"message" bson:"message"`
	AuthorID       string             `json:"author_id" bson:"author_id"`
	GraphqlStatus  string             `json:"graphql_status" bson:"graphql_status"`
	RabbitMQStatus string             `json:"rabbitmq_status" bson:"rabbitmq_status"`
	RedisStatus    string             `json:"redis_status" bson:"redis_status"`
	GrpcStatus     string             `json:"grpc_status" bson:"grpc_status"`
}
