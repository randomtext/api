package responses

type WatchData struct {
	OperationType string `bson:"operation_type" json:"operation_type"`
	Text          *Text  `bson:"text" json:"text"`
	CollName      string `bson:"coll_name" json:"coll_name"`
}

type SubscriberData struct {
	CollName string `bson:"coll_name" json:"coll_name"`
	AuthorID string `bson:"author_id" json:"author_id"`
}
